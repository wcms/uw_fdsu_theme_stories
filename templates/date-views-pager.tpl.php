<?php

/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * - $plugin: The pager plugin object. This contains the view as well as a lot.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   URLs for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 *
 * $block:
 *   Whether or not this view is in a block.
 *
 * $view
 *   The view object for this navigation.
 *
 * $pager_prefix
 *   Additional information that might be
 *   added by overriding template_process_date_views_pager().
 *
 * $extra_classes
 *   Extra classes for the wrapper, if any.
 */
?>

<?php print !empty($pager_prefix) ? $pager_prefix : ''; ?>
<div class="date-nav-wrapper clearfix<?php print !empty($extra_classes) ? $extra_classes : ''; ?>">
  <div class="date-nav">
    <div class="date-prev">
      <?php if (!empty($prev_url)) : ?>
        <?php
        $text = '&laquo;' . ($mini ? '' : t('Previous', array(), array('context' => 'date_nav')));
        print l($text, $prev_url, $prev_options);
        ?>
      <?php endif; ?>
    </div>
    <div class="date-heading">
      <h2><?php print $nav_title ?></h2>
    </div>
    <div class="date-next">
    <?php if (!empty($next_url)) : ?>
      <?php print l(($mini ? '' : t('Next', array(), array('context' => 'date_nav'))) . ' &raquo;', $next_url, $next_options); ?>
    <?php endif; ?>
    </div>
  </div>
</div>
