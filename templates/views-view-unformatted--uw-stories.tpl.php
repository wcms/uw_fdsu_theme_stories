<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup view
 */
?>

<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?> clearfix">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
