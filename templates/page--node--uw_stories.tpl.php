<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div class="breakpoint"></div>

<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site--stories uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <div class="uw-section--inner">
      <nav id="skip" class="skip" aria-label="Skip links">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </nav>
    </div>
    <header id="header" class="uw-header--global">
      <?php
      $global_message = file_get_contents('https://uwaterloo.ca/global-message.html');
      if ($global_message) {
        ?>
        <div id="global-message">
          <?php echo $global_message; ?>
        </div>
        <?php
      }
      ?>
      <div class="uw-section--inner">

        <?php print render($page['global_header']); ?>
      </div>
    </header>
    <div id="site--offcanvas" class="uw-site--off-canvas <?php print ($uw_theme_branding === 'full') ? 'non_generic_header' : 'generic_header'; ?>">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <?php if ($uw_theme_branding === 'full') { ?>
      <div id="site-colors" class="uw-site--colors">
        <div class="uw-section--inner">
          <div class="uw-site--cbar">
            <div class="uw-site--c1 uw-cbar"></div>
            <div class="uw-site--c2 uw-cbar"></div>
            <div class="uw-site--c3 uw-cbar"></div>
            <div class="uw-site--c4 uw-cbar"></div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div id="site-header" class="uw-site--header" role="region" aria-label="Site title">
      <div class="uw-section--inner">
        <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
          <?php print $site_name; ?>
          <?php if(isset($site_slogan) && $site_slogan !== ""): ?>
            <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
          <?php endif; ?>
        </a>
      </div>
    </div>

    <div class="uw-stories--main">
      <div class="uw-section--inner">
        <div id="content">
          <div class="stories_featured_image hero">
          <?php if ((isset($story_type)) && isset($story_landing_nid)): ?>
            <?php print l($story_type, 'node/' . $story_landing_nid, array('attributes' => array('class' => 'story-type'))); ?>
          <?php elseif (isset($story_type)): ?>
            <span class="story-type">
            <?php print $story_type; ?>
          </span>
          <?php endif; ?>
          <?php if($content['use_featured_image']): ?>
            <?php if($content['feature_image']['uri'] !== NULL): ?>
            <picture class="stories_featured_img">
              <source srcset="<?php print $content['feature_image_style']['xlarge']; ?>" media="(min-width: 1280px)">
              <!--<source srcset="<?php print $content['feature_image_style']['large']; ?>" media="(min-width: 1010)"> -->
              <source srcset="<?php print $content['feature_image_style']['large']; ?>" media="(min-width: 480px)">
              <source srcset="<?php print $content['feature_image_style']['small']; ?>" media="(min-width: 320px)">
              <source srcset="<?php print $content['feature_image_style']['square']; ?>">
              <img src="<?php print $content['feature_image_style']['large']; ?>" alt="<?php print $content['feature_image']['alt']; ?>">
            </picture>
            <?php endif;?>
          <?php endif;?>
          <div class="article-info <?php
          print $content['use_featured_image'] ? 'use' : 'no-use';
          ?>">
              <span class="article-date"><?php print $content['date_created']; ?></span>
              <h1<?php print $title_attributes;?>><?php print $title; ?></h1>
              <?php print $content['feature_field_subhead']; ?>
              <span class="author">By <?php print $content['feature_author']; ?></span>
              <span class="author-position"><?php print $content['feature_author_position']; ?></span>
            </div>
          </div>
          <?php print render($page['banner']); ?>
        </div>
      </div>
    </div>
    <div id="main" class="uw-site--main" role="main">
      <div class="uw-section--inner">
        <nav id="site-navigation-wrapper" class="uw-site-navigation--wrapper" aria-label="Site">
          <div id="site-specific-nav" class="uw-site-navigation--specific">
            <div id="site-navigation" class="uw-site-navigation">
              <?php if (user_access('access content')) {
                print render($page['sidebar_first']);
              } ?>
            </div>
          </div>
        </nav>
        <div class="uw-site--main-top">
          <div class="uw-site--banner">
            <?php print render($page['banner']); ?>
          </div>
          <div class="uw-site--messages">
            <?php print $messages; ?>
          </div>

          <div class="uw-site--help">
            <?php print render($page['help']); ?>
          </div>

          <!-- when logged in -->
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>

          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>
        <div class="uw-site-main--content page-type-uw-stories">
          <div id="content" class="uw-site-content">

            <input type="checkbox" id="article-layout-dir" class="layout-dir-check"/>
            <?php print render($page['content']); ?>
          </div><!--/main-content-->
          <?php $sidebar = render($page['sidebar_second']); ?>
          <?php $sidebar_promo = render($page['promo']); ?>

            <?php if(isset($sidebar) || isset($sidebar_promo) || isset($content['feature_field_sidebar'])) : ?>

              <?php if(($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '') || ($content['feature_field_sidebar'] !== NULL && $content['feature_field_sidebar'] !== '')) : ?>
              <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">

                <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                  <div id="site-sidebar" class="uw-site-sidebar <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') :?> <?php  echo 'sticky-promo';?><?php
                 endif; ?>">

                  <?php if (isset($sidebar_promo)) : ?>
                  <?php if ($sidebar_promo !== NULL && $sidebar_promo !== ''): ?>
                    <div class="uw-site-sidebar--promo">
                      <?php print render($page['promo']); ?>
                    </div>
                  <?php endif; ?>
                  <?php endif; ?>
                    <div class="uw-site-sidebar--second">
                      <?php print render($page['sidebar_second']); ?>

                    </div>
                    <div class="uw-site-sidebar--comp">
                      <?php print ($content['feature_field_sidebar']); ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php endif; ?>
            <?php endif; ?>
        </div>
      </div><!--/section inner-->
    </div><!--/site main-->
  <div id="footer" class="uw-footer" role="contentinfo">
    <div id="uw-site-share" class="uw-site-share">
    <div class="uw-section--inner">
      <div class="uw-section-share"></div>
      <ul class="uw-site-share-top">
      <li class="uw-site-share--button__top">
        <a href="#main" id="uw-top-button" class="uw-top-button">
        <span class="ifdsu fdsu-arrow"></span>
        <span class="uw-footer-top-word">TOP</span>
        </a>
      </li>
      <li class="uw-site-share--button__share">
        <a href="#uw-site-share" class="uw-footer-social-button">
        <span class="ifdsu fdsu-share"></span>
        <span class="uw-footer-share-word">Share</span>
        </a>
      </li>
      </ul>
    </div>
    </div>
    
      <?php include(__DIR__ . '/site-footer.tpl.php'); ?>
    <?php print render($page['global_footer']); ?>
  </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
