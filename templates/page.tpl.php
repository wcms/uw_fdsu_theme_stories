<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */

  $path = $_SERVER['REQUEST_URI'];
  $find = 'archive';
  $pos = strpos($path, $find);
  $uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<?php if ($pos !== FALSE): ?>
  <?php $pos = "archive";?>
<?php endif; ?>
<div class="breakpoint"></div>

<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site uw-site--stories <?php print $pos; ?>"<?php print $attributes; ?>>
<div class="uw-site--inner">
    <div class="uw-section--inner">
      <nav id="skip" class="skip" aria-label="Skip links">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </nav>
    </div>
    <header id="header" class="uw-header--global">
      <?php
      $global_message = file_get_contents('https://uwaterloo.ca/global-message.html');
      if ($global_message) {
        ?>
        <div id="global-message">
          <?php echo $global_message; ?>
        </div>
        <?php
      }
      ?>
      <div class="uw-section--inner">
        <?php print render($page['global_header']); ?>
      </div>
    </header>
    <div id="site--offcanvas" class="uw-site--off-canvas <?php print ($uw_theme_branding === 'full') ? 'non_generic_header' : 'generic_header'; ?>">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <?php if ($uw_theme_branding === 'full') { ?>
      <div id="site-colors" class="uw-site--colors">
        <div class="uw-section--inner">
          <div class="uw-site--cbar">
            <div class="uw-site--c1 uw-cbar"></div>
            <div class="uw-site--c2 uw-cbar"></div>
            <div class="uw-site--c3 uw-cbar"></div>
            <div class="uw-site--c4 uw-cbar"></div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div id="site-header" class="uw-site--header" role="region" aria-label="Site title">
      <div class="uw-section--inner">
        <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
          <?php print $site_name; ?>

          <?php if (isset($site_slogan) && $site_slogan !== ""):  ?>
            <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
          <?php endif; ?>
        </a>
        <?php if (isset($story_type) &&  (!drupal_is_front_page())) : ?>
          <span class="uw-site—subtitle"> <?php print $story_type; ?> </span>
        <?php endif; ?>

      </div>
    </div>
    <?php if ($page['banner']): ?>
      <div class="uw-stories--main">
            <div class="uw-section--inner">
              <div id="content">
                <?php print render($page['banner']); ?>
              </div>
          </div>
      </div>
    <?php endif; ?>
    <div id="main" class="uw-site--main" role="main">
      <div class="uw-section--inner">
        <div class="uw-site--main-top">
          <div class="uw-site--messages">
              <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
              <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
              <?php
// Print $breadcrumb;. ?>
          </div>
          <div class="uw-site--title">
            <!-- don't print the title on the front page or on the Featured Stories nodes ? -->
            <?php if (!drupal_is_front_page() && !(isset($node) && $node->type == 'stories_featured')) : ?>
              <h1>
              <?php if ($title != "uWaterloo Stories") :?>
                <?php print $title ? $title : $site_name;?>
                <?php else: ?>
                <?php print '<div id="uw-story">' . $title . '</div>';?>
                <?php endif; ?>
              </h1>
            <?php endif; ?>
          </div>
          <!-- when logged in -->
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div><?php
          endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
        </div>

        <div class="uw-site-main--content">
          <div id="content" class="uw-site-content">
              <?php if(isset($show_layout) && $show_layout) :?>
                <input type="checkbox" id="article-layout-dir" class="layout-dir-check"/>
                <div class="article-list-layout">
                  <label for="article-layout-dir" class="article-layout-button">
                    <span class="article-layout-list"><span class="ifdsu fdsu-list-view"></span> List </span>
                    <span class="article-layout-grid"><span class="ifdsu fdsu-grid-view" ></span> Grid </span>
                  </label>
                </div>
              <?php endif; ?>
              <?php
                print render($page['content']);
              ?>
          </div>
        </div>
      </div>
      <!--/section inner-->
    </div>
    <!--/site main-->
    <div id="footer" class="uw-footer" role="contentinfo">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share"></div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
                <span class="ifdsu fdsu-arrow"></span>
                <span class="uw-footer-top-word">TOP</span>
              </a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
                <span class="ifdsu fdsu-share"></span>
                <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <?php include(__DIR__ . '/site-footer.tpl.php'); ?>
      <?php print render($page['global_footer']); ?>
     </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
